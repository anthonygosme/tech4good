Tech4Good Suite
----------

# Build project 
Use gradle wrapper :
```shell script
./gradlew build
```

After build, you can launch the full suite using : 

```shell script
 docker-compose -f /compose/docker-compose.yaml up 
```

Take a look at the docker-compose file to see which port are exposed.
## Back End 

Node Js backend with Express framework 
Start with package.json script or : 
```shell script
./gradlew runBack
```

## Front Admin

Start with package.json script or : 
```shell script
./gradlew runAdmin
```

## Front Public 

Start with package.json script or : 
```shell script
./gradlew runPublic
```
