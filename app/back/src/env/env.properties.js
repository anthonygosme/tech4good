const env = process.env.NODE_ENV || "dev"; // 'dev' or 'docker'
console.log("Using environement : "+ env);

const docker = {
    mongoUri: "mongodb://mongo:27017/"
};
const dev = {
    mongoUri: "mongodb://127.0.0.1:27017/"
};

const config = {
    docker,
    dev
};

module.exports = config[env.trim()];
