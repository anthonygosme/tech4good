const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const questionnaireRoutes = require("./routes/questionnaire");
const userRoutes = require("./routes/user");

const app = express();

const config = require("./env/env.properties")

mongoose
  .connect(
      config.mongoUri
  )//mongo "mongodb+srv://cluster0-syeo3.mongodb.net/<dbname>" --username Andromak
  .then(() => {
    console.log("Connected to database!");
  })
  .catch(() => {
    console.log("Connection failed!");
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/images", express.static(path.join("/images")));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});

app.use("/api/quiz", questionnaireRoutes);
app.use("/api/user", userRoutes);

module.exports = app;
