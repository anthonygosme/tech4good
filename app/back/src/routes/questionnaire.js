const express = require("express");

const QuestionnaireController = require("../controllers/questionnaire");

const checkAuth = require("../middleware/check-auth");
const extractFile = require("../middleware/file");

const router = express.Router();

router.post("", extractFile, QuestionnaireController.createQuestionnaire);

router.put("/:id", checkAuth, extractFile, QuestionnaireController.updateQuestionnaire);

router.get("", QuestionnaireController.getQuestionnaires);

/*
router.get("/:id", QuestionnaireController.getQuestionnaire);
*/

router.delete("/:id", checkAuth, QuestionnaireController.deleteQuestionnaire);

module.exports = router;
