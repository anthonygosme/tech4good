const Questionnaire = require("../models/mongoose-questionnaire");

exports.createQuestionnaire = (req, res, next) => {
    const questionnaire = new Questionnaire(req.body);
    questionnaire.save()
        .then(createdQuestionnaire => {
            res.status(201).json({
                message: "Questionnaire added successfully",
                createdQuestionnaire
            });
        })
        .catch(error => {
            res.status(500).json({
                message: "Creating a Questionnaire failed!"
            });
        });
};


exports.updateQuestionnaire = (req, res, next) => {
    const questionnaire = new Questionnaire(req.body);
    console.log(questionnaire);
    questionnaire
        .updateOne({_id: questionnaire._id}, questionnaire)
        .then(updatedQuestionnaire => {
            res.status(201).json({
                message: "Questionnaire updated successfully",
                updatedQuestionnaire
            });
        })
        .catch(error => {
            res.status(500).json({
                message: "Updating a Questionnaire failed!"
            });
        });
};

exports.getQuestionnaires = (req, res, next) => {

    const questionnaireQuery = Questionnaire.find();
    let fetchedQuestionnaires;
    questionnaireQuery
        .then(documents => {
            fetchedQuestionnaires = documents;
            return Questionnaire.count();
        })
        .then(count => {
            res.status(200).json({
                message: "Posts fetched successfully!",
                questionnaires: fetchedQuestionnaires,
                maxPosts: count
            });
        })
        .catch(error => {
            res.status(500).json({
                message: "Fetching posts failed!"
            });
        });
};

/*exports.getQuestionnaire = (req, res, next) => {
    Questionnaire.findById(req.params.id)
        .then(questionnaire => {
            if (questionnaire) {
                res.status(200).json(questionnaire);
            } else {
                res.status(404).json({message: "Post not found!"});
            }
        })
        .catch(error => {
            res.status(500).json({
                message: "Fetching post failed!"
            });
        });
};*/

exports.deleteQuestionnaire = (req, res, next) => {
    Questionnaire.deleteOne({_id: req.params.id})
        .then(result => {
            console.log(result);
            if (result.n > 0) {
                res.status(200).json({message: "Deletion successful!"});
            } else {
                res.status(401).json({message: "Not authorized!"});
            }
        })
        .catch(error => {
            res.status(500).json({
                message: "Deleting posts failed!"
            });
        });
};
