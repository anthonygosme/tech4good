const mongoose = require("mongoose");

const postSchema = mongoose.Schema({
  question: { type: String, required: true },
  answers: { type: Array, required: true },
  weights:{ type: Array, required: true},
  creator: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true }
});

module.exports = mongoose.model("Questionnaire", postSchema);
