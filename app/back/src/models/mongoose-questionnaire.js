const mongoose = require("mongoose")

const reponseSchema = mongoose.Schema({
    _text : {type :String, required: true},
    _weight : {type : Number, required : true}
})

const questionSchema = mongoose.Schema({
    _text : {type :String, required: true},
    _responses : [reponseSchema]

})
const questionnaireSchema = mongoose.Schema({
    _name : {type :String, required: true},
    _questions : [questionSchema]

})

module.exports = mongoose.model("Questionnaire",questionnaireSchema);
