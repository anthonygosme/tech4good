import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

import {QuestionnaireService} from '../questionnaire.service';
import {AuthService} from '../../auth/auth.service';
import {Questionaire} from '../model/questionaire.model';
import {Reponse} from '../model/reponse.model';
import {Question} from '../model/question.model';

@Component({
    selector: 'app-post-create',
    templateUrl: './questionnaire.component.html',
    styleUrls: ['./questionnaire.component.css']
})
export class QuestionnaireComponent implements OnInit, OnDestroy {

    isLoading = false;
    name: any;

    private authStatusSub: Subscription;

    questionnaire: Questionaire;
    questionnaires: Array<Questionaire>;

    constructor(
        public questionnaireService: QuestionnaireService,
        public route: ActivatedRoute,
        private authService: AuthService
    ) {
    }

    ngOnInit() {
        this.authStatusSub = this.authService
            .getAuthStatusListener()
            .subscribe(authStatus => {
                this.isLoading = false;
            });

        this.questionnaireService.getQuestionnaires()
            .subscribe(questionnaires => {
                console.log(questionnaires);
                this.questionnaires = questionnaires;
            });
    }

    createNewQuiz() {
        this.questionnaire = new Questionaire();
        this.questionnaires.push(this.questionnaire);

    }

    addQuestion() {
        let question = new Question();
        let answer = new Reponse();
        question.responses.push(answer);
        this.questionnaire.questions.push(question);
    }

    addAnswer(question: Question) {
        question.responses.push(new Reponse());
    }

    removeAnswer(question: Question) {
        if (question.responses.length > 1) {
            question.responses.pop();
        }
    }


    onSaveQuestionnaire() {
        console.log(this.questionnaire);
        console.log(this.questionnaire.name);
            if (this.questionnaire.id) {
              this.questionnaireService.updateQuestionnaire(
                  this.questionnaire,
              );
            } else {
              this.questionnaireService.addQuestionnaire(this.questionnaire);
              //TODO reinint dropdown values
            }

    }

    ngOnDestroy() {
        this.authStatusSub.unsubscribe();
    }

}
