import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {QuestionnaireComponent} from './component/questionnaire.component';
import {AngularMaterialModule} from '../angular-material.module';
import {MatListModule} from '@angular/material/list';
import {AuthModule} from '../auth/auth.module';

@NgModule({
  declarations: [QuestionnaireComponent],
  imports: [
    AuthModule,
    CommonModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    RouterModule,
    FormsModule,
    MatListModule
  ]
})
export class QuestionnaireModule {
}
