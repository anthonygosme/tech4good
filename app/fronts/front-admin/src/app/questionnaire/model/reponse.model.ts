export class Reponse {
    private _text: string;
    private _weight: number;

    constructor() {
    }

    get text(): string {
        return this._text;
    }

    get weight(): number {
        return this._weight;
    }

    set text(value: string) {
        this._text = value;
    }

    set weight(value: number) {
        this._weight = value;
    }
}
