import {Reponse} from "./reponse.model";

export class Question {
    private _text: string;
    private _responses: Array<Reponse>= new Array<Reponse>();

    constructor() {
    }

    get text(): string {
        return this._text;
    }

    set text(value: string) {
        this._text = value;
    }

    get responses(): Array<Reponse> {
        return this._responses;
    }

    set responses(value: Array<Reponse>) {
        this._responses = value;
    }
}
