import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';

import {environment} from '../../environments/environment';
import {Questionaire} from './model/questionaire.model';


const BACKEND_URL = environment.apiUrl + '/quiz/';

@Injectable({providedIn: 'root'})
export class QuestionnaireService {

    private questionnaires: Questionaire[] = [];
    private questionnaireUpdated = new Subject<{ posts: Questionaire[]; questionnaireCount: number }>();

    constructor(private http: HttpClient, private router: Router) {
    }

    getQuestionnaires() {
        return this.http
            .get<{ message: string; questionnaires: any; }>(
                BACKEND_URL
            )
            .pipe(
                map(jsonQuestionnaires => {
                    this.questionnaires = jsonQuestionnaires.questionnaires;
                    console.log(this.questionnaires);
                    return this.questionnaires;
                    //TODO map to Questionnaire.ts
                })
            );

    }

    getPostUpdateListener() {
        return this.questionnaireUpdated.asObservable();
    }

    getQuestionnaire(id: string) {
        return this.http.get<{
            _id: string;
            title: string;
            content: string;
            creator: string;
        }>(BACKEND_URL + id);
    }

    addQuestionnaire(questionnaire: Questionaire) {
        console.log(questionnaire);
        this.http
            .post<{ message: string; questionnaire: Questionaire }>(
                BACKEND_URL,
                questionnaire
            )
            .subscribe(responseData => {
                this.router.navigate(['/']);
            });
    }

    updateQuestionnaire(questionnaire: Questionaire) {

        this.http
            .put(BACKEND_URL + questionnaire.id, questionnaire)
            .subscribe(response => {
                this.router.navigate(['/']);
            });
    }

    deletePost(postId: string) {
        return this.http.delete(BACKEND_URL + postId);
    }
}
