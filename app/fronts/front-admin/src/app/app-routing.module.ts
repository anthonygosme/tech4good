import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { QuestionnaireComponent } from "./questionnaire/component/questionnaire.component";
import { AuthGuard } from "./auth/auth.guard";

const routes: Routes = [
  { path: "", component: QuestionnaireComponent },
  { path: "auth", loadChildren: "./auth/auth.module#AuthModule"},
  { path: "create", component: QuestionnaireComponent, canActivate: [AuthGuard] },
  { path: "edit/:postId", component: QuestionnaireComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule {}
