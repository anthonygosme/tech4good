import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Injector, NgModule} from '@angular/core';
import {AngularMaterialModule} from './angular-material.module';
import {DialogWrapper, QuestionnairePublicComponent} from './questionnaire/component/questionnaire-public.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {createCustomElement} from '@angular/elements';
import {MatListModule} from '@angular/material';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';

@NgModule({

  declarations: [
    QuestionnairePublicComponent,
    DialogWrapper
  ],
  imports: [
    HttpClientModule,
    CommonModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    RouterModule,
    FormsModule,
    MatListModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
  ],
  entryComponents: [DialogWrapper,QuestionnairePublicComponent]
})
export class AppModule {

  constructor(private injector: Injector) {
  }

  ngDoBootstrap() {
    const el = createCustomElement(DialogWrapper, {injector: this.injector});
    customElements.define('quiz-button', el);
  }

}
