import {FilledReponse} from "./reponse.filled.model";

export class FilledQuestion {
    private _text: string;
    private _responses: Array<FilledReponse>= new Array<FilledReponse>();

    constructor() {
    }

    get text(): string {
        return this._text;
    }

    set text(value: string) {
        this._text = value;
    }

    get responses(): Array<FilledReponse> {
        return this._responses;
    }

    set responses(value: Array<FilledReponse>) {
        this._responses = value;
    }
}
