export class Company {
    private _name: string;
    private _employeeNumber: number;

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get employeeNumber(): number {
        return this._employeeNumber;
    }

    set employeeNumber(value: number) {
        this._employeeNumber = value;
    }
}
