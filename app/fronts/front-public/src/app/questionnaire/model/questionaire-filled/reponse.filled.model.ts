export class FilledReponse {
    private _text: string;
    private _weight: number;
    private _selected: boolean;

    constructor() {
    }

    get text(): string {
        return this._text;
    }

    get weight(): number {
        return this._weight;
    }

    set text(value: string) {
        this._text = value;
    }

    set weight(value: number) {
        this._weight = value;
    }

    get selected(): boolean {
        return this._selected;
    }

    set selected(value: boolean) {
        this._selected = value;
    }
}
