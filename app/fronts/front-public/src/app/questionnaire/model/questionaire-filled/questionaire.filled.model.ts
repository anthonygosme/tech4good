import {FilledQuestion} from './question.filled.model';
import {ContactInfo} from './contact-info.model';
import {Company} from './company.model';

export class FilledQuestionnaire {

    private _name: string;
    private _questions: Array<FilledQuestion> = new Array<FilledQuestion>();
    private _id: any;
    private _originalQuestionnaireId: any;
    private _acceptedRGPD: boolean;
    private _company: Company;
    private _contactInfos: ContactInfo;

    constructor() {
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get questions(): Array<FilledQuestion> {
        return this._questions;
    }

    set questions(value: Array<FilledQuestion>) {
        this._questions = value;
    }

    get id(): any {
        return this._id;
    }

    set id(value: any) {
        this._id = value;
    }

    get originalQuestionnaireId(): any {
        return this._originalQuestionnaireId;
    }

    set originalQuestionnaireId(value: any) {
        this._originalQuestionnaireId = value;
    }

    get company(): Company {
        return this._company;
    }

    set company(value: Company) {
        this._company = value;
    }

    get contactInfos(): ContactInfo {
        return this._contactInfos;
    }

    set contactInfos(value: ContactInfo) {
        this._contactInfos = value;
    }

    get acceptedRGPD(): boolean {
        return this._acceptedRGPD;
    }

    set acceptedRGPD(value: boolean) {
        this._acceptedRGPD = value;
    }
}
