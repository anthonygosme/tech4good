import {Question} from "./question.model";

export class Questionaire {
    private _name: string;
    private _questions: Array<Question> = new Array<Question>();
    private _id: any;

    constructor() {
    }


    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get questions(): Array<Question> {
        return this._questions;
    }

    set questions(value: Array<Question>) {
        this._questions = value;
    }

    get id(): any {
        return this._id;
    }

    set id(value: any) {
        this._id = value;
    }
}
