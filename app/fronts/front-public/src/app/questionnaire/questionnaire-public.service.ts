import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';
import {map} from 'rxjs/operators';

import {environment} from '../../environments/environment';
import {FilledQuestionnaire} from './model/questionaire-filled/questionaire.filled.model';
import {Questionaire} from './model/questionaire/questionaire.model';


const BACKEND_URL = environment.apiUrl + '/quiz/';

@Injectable({providedIn: 'root'})
export class QuestionnairePublicService {

    private questionnaires: FilledQuestionnaire[] = [];
    private questionnaireUpdated = new Subject<{ posts: FilledQuestionnaire[]; questionnaireCount: number }>();

    constructor(private http: HttpClient) {
    }

    getQuestionnaires() {
        return this.http
            .get<{ message: string; questionnaires: any; }>(
                BACKEND_URL
            )
            .pipe(
                map(jsonQuestionnaires => {
                    return Array<FilledQuestionnaire>();
                    //TODO map to Questionnaire.ts
                })
            );

    }

    getPostUpdateListener() {
        return this.questionnaireUpdated.asObservable();
    }

    getQuestionnaireTemplate(id: string) {
        return this.http.get <Questionaire>(BACKEND_URL + id);
    }

    addQuestionnaire(questionnaire: FilledQuestionnaire) {
        this.http
            .post<{ message: string; questionnaire: FilledQuestionnaire }>(
                BACKEND_URL,
                questionnaire
            )
            .subscribe(responseData => {

            });
    }

    updateQuestionnaire(questionnaire: FilledQuestionnaire) {

        this.http
            .put(BACKEND_URL + questionnaire.id, questionnaire)
            .subscribe(response => {

            });
    }

    deletePost(postId: string) {
        return this.http.delete(BACKEND_URL + postId);
    }
}
