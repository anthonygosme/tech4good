import {Component, OnDestroy, OnInit} from '@angular/core';

import {QuestionnairePublicService} from '../questionnaire-public.service';
import {FilledQuestionnaire} from '../model/questionaire-filled/questionaire.filled.model';
import {Questionaire} from '../model/questionaire/questionaire.model';
import {Question} from '../model/questionaire/question.model';
import {Reponse} from '../model/questionaire/reponse.model';
import {FilledQuestion} from '../model/questionaire-filled/question.filled.model';
import {FilledReponse} from '../model/questionaire-filled/reponse.filled.model';
import {Company} from '../model/questionaire-filled/company.model';
import {ContactInfo} from '../model/questionaire-filled/contact-info.model';
import {MatDialog} from '@angular/material';

@Component({
    template: '<button mat-stroked-button color = "primary" (click)="openDialog()">Start Questionnaire</button>',
})
export class DialogWrapper {
    constructor(public dialog: MatDialog) {}
    openDialog() {
        const dialogRef = this.dialog.open(QuestionnairePublicComponent);
        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result: ${result}`);
        });
    }
}

@Component({
    templateUrl: './questionnaire-public.component.html',
    styleUrls: ['./questionnaire-public.component.css']
})
export class QuestionnairePublicComponent implements OnInit, OnDestroy {

    stepNumberBeforeQuizzStart: number = 10;
    questionnaireTemplate: Questionaire;
    filledQuestionnaire: FilledQuestionnaire;

    constructor(
        public questionnaireService: QuestionnairePublicService
    ) {
    }

    ngOnInit() {
        //TODO get template questionnaire
        this.filledQuestionnaire = this.buildFrom(this.buildDummyQuestionnaire());
    }


    ngOnDestroy() {
        //TODO also save on destroy
    }

    private buildFrom(questionnaireTemplate: Questionaire) {
        const filledQuestionaire: FilledQuestionnaire = new FilledQuestionnaire();
        filledQuestionaire.company = new Company();
        filledQuestionaire.contactInfos = new ContactInfo();
        filledQuestionaire.originalQuestionnaireId = questionnaireTemplate.id;
        filledQuestionaire.name = questionnaireTemplate.name;
        questionnaireTemplate.questions.forEach(templateQuestion => {
            const filledQuestion: FilledQuestion = new FilledQuestion();
            filledQuestion.text = templateQuestion.text;
            templateQuestion.responses.forEach(templateAnswer => {
                const filledAnswer: FilledReponse = new FilledReponse();
                filledAnswer.text = templateAnswer.text;
                filledAnswer.weight = templateAnswer.weight;
                filledAnswer.selected = false;
                filledQuestion.responses.push(filledAnswer);
            });
            filledQuestionaire.questions.push(filledQuestion);
        });
        return filledQuestionaire;
    }

    handleAnswerClicked(filledQuestion: FilledQuestion, filledAnswer: FilledReponse) {
        filledQuestion.responses.forEach(reponse => {
            reponse.selected = false;
        });
        filledAnswer.selected = true;
    }

    private buildDummyQuestionnaire() {
        let template: Questionaire = new Questionaire();
        template.name = 'Test Quest';

        for (let i = 0; i < 2; i++) {
            let templaQues = new Question();
            templaQues.text = 'Do you lift';

            let templaAnswer: Reponse = new Reponse();
            templaAnswer.text = 'Sure bro';
            templaAnswer.weight = 2;

            let templaAnswer2: Reponse = new Reponse();
            templaAnswer2.text = 'Nope bro';
            templaAnswer2.weight = 1;
            templaQues.responses.push(templaAnswer, templaAnswer2);
            template.questions.push(templaQues);
        }


        return template;
    }

    haveOneAnswerSelected(filledQuestion: FilledQuestion) {
        let haveOneSelected: boolean = false;
        filledQuestion.responses.forEach(answer => {
            haveOneSelected = haveOneSelected || answer.selected;
        });
        return haveOneSelected;
    }

    handleRGPDClicked(accepted: boolean) {
        this.filledQuestionnaire.acceptedRGPD = accepted;
    }

    calculateQuestionnaireScorePerCent(filledQuestionnaire: FilledQuestionnaire) {
        let score = 0;
        let max_score = 0;
        filledQuestionnaire.questions.forEach(question => {
            let maxForQuestion = 0;
            question.responses.forEach(reponse => {
                if (reponse.weight > maxForQuestion) {
                    maxForQuestion = reponse.weight;
                }
                if (reponse.selected) {
                    score += reponse.weight;
                }
            });
            max_score += maxForQuestion;
        });
        return score / max_score * 100;
    }
}
